from distutils.command import register
from django import template

register = template.Library()

@register.filter('get_value_from_dict')
def get_value_from_dict(dict_data, key):
    if key:
        return dict_data.get('question'+str(key))

@register.filter('compare_lists')
def get_value_from_dict(list1,list2):
    if list2:
        return (set(list1)==set(list2))