    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    function deleteThing(btn) {
        btn.closest('div').remove();
    }

    $('.deleteBtn').on('click', function () {
        $(this).closest('div').remove();
    });

    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/\{(\d+)\}/g, function (m, n) {
            return args[n];
        });
    };

    var count = 0;

    function addQuestion(questionType) {
        var questionId = 'question' + count;
        if (questionType != 'text') {
            var question = $('#hidden-template-QSTN-RB-CB').html().format(questionId, questionType);
        } else {
            var question =$('#hidden-template-QSTN-TXT').html().format(questionId, questionType);
        }

        $("#quizContainer").append(question);
        count++;
    }

    function addAnswer(questionId, questionType) {
        var question = $('#hidden-template-ANSW').html().format(questionType, questionId);
        $('#' + questionId).append(question);

    }


    function quizJSON() {
        var quiz = {};
        quizname = $('#quizName').val();
        var quizDict = [];
        var rightAns = [];

        $('div.question').each(function () {
            var question = $(this).find(".question");
            var current = question.val();
            quiz[current] = {};
            quiz[current]['type'] = $(this).attr('type');
            quiz[current]['answers'] = [];
            quiz[current]['correctAnswers'] = [];

            $(this).find('.answer').each(function () {
                if ($(this).is(':checked')) {
                    quiz[current]['correctAnswers'].push($(this).closest('div').find(".answerText").val())
                }
            })


            $(this).find('.answerText').each(function () {
                if (question.attr('type') === 'open') {
                    quiz[current]['correctAnswers'].push($(this).val());
                }
                quiz[current]['answers'].push($(this).val());
            });
        })
        return [quizname, quiz];
    }

    function sendRequest() {
        var quizinstance = quizJSON();
        // JSON.stringify(quiz)
        if (isDataValid(quizinstance[1]) && /\S/.test(quizinstance[0])) {
            $.ajax({
                type: 'POST',
                url: '/quiz/save',
                data: {
                    quiz_data: JSON.stringify(quizinstance[1]),
                    quiz_name: quizinstance[0],
                },
                success: function (response) {
                    window.location = "/";

                }
            });
        } else {
            alert("fulfill the from plaes")
        }


    }

    function isDataValid(obj) {
        var mandatoryKeys = ['answers', 'correctAnswers'];
        var isValid = true;
        if (!jQuery.isEmptyObject(obj)) {
            Object.keys(obj).forEach(function (key) {
                mandatoryKeys.forEach(function (value) {
                    if (obj[key][value] === "" || !obj[key][value].length > 0) {
                        isValid = false;
                    }
                });
            });
            return isValid;
        }
    }